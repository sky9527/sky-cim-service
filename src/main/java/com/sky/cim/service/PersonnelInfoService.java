package com.sky.cim.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sky.cim.bean.ResultData;
import com.sky.cim.entity.PersonnelInfoEntity;
import com.sky.cim.mapper.PersonnelInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Slf4j
@Service
public class PersonnelInfoService extends ServiceImpl<PersonnelInfoMapper, PersonnelInfoEntity> {
    @Resource
    private PersonnelInfoMapper personnelInfoMapper;

    public ResultData getPersonnelInfoList(HttpServletRequest request, String pageNo, String pageSize) {
        try {
            String companyName = request.getParameter("companyName");
            String name = request.getParameter("name");
            String orderType = request.getParameter("orderType");
            long current = Long.parseLong(pageNo);
            long size = Long.parseLong(pageSize);
            QueryWrapper<PersonnelInfoEntity> queryWrapper = new QueryWrapper<>();
            if (StringUtils.isNotBlank(companyName)) {
                queryWrapper.like("company_name", "%".concat(companyName).concat("%"));
            }
            if (StringUtils.isNotBlank(name)) {
                queryWrapper.like("name", "%".concat(name).concat("%"));
            }
            Page<PersonnelInfoEntity> page = new Page<>();
            if (current > 0 && size > 0) {
                page.setCurrent(current);
                page.setSize(size);
            }
//            Page<PersonnelInfoEntity> personnelInfoEntityPage = personnelInfoMapper.selectPage(page, queryWrapper);
            Page<PersonnelInfoEntity> personnelInfoEntityPage = personnelInfoMapper.selectPersonnelInfoPage(page, companyName, name, orderType);
            return ResultData.success(personnelInfoEntityPage);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.fail(e.getMessage());
        }
    }

}
