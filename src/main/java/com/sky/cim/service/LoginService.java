package com.sky.cim.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sky.cim.bean.ResultData;
import com.sky.cim.entity.UserInfoEntity;
import com.sky.cim.mapper.UserInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Service
public class LoginService extends ServiceImpl<UserInfoMapper, UserInfoEntity> {
    @Resource
    private UserInfoMapper userInfoMapper;

    public ResultData login(HttpServletRequest request, JSONObject jsonObject) {
        try {
            String username = jsonObject.getString("username");
            String password = jsonObject.getString("password");
            ConcurrentHashMap<String, Object> queryMap = new ConcurrentHashMap<>(1);
            queryMap.put("username", username);
            List<UserInfoEntity> userInfoEntities = userInfoMapper.selectByMap(queryMap);
            if (userInfoEntities.size() < 1) {
                return ResultData.fail("登录失败,用户不存在!");
            }
            UserInfoEntity userInfoEntity = userInfoEntities.get(0);
            if (password.equals(userInfoEntity.getPassword())) {
                HttpSession session = request.getSession();
                session.setAttribute("user", username);
                session.setMaxInactiveInterval(3600 * 6);
                return ResultData.success("登录成功!");
            } else {
                return ResultData.fail("登录失败,密码错误!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.fail("登录失败!".concat(e.getMessage()));
        }
    }

    public ResultData logout(HttpServletRequest request) {
        try {
            Object user = request.getSession().getAttribute("user");
            if (Objects.nonNull(user)) {
                request.getSession().removeAttribute("user");
            }
            return ResultData.success("退出登录成功!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.fail("退出登录失败!".concat(e.getMessage()));
        }
    }

    public ResultData checkLogin(HttpServletRequest request) {
        try {
            HttpSession session = request.getSession();
            Object user = session.getAttribute("user");
            if (Objects.nonNull(user)) {
                return ResultData.success("valid");
            } else {
                return ResultData.success("invalid");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.fail("校验登录状态失败!".concat(e.getMessage()));
        }
    }
}
