package com.sky.cim.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sky.cim.bean.ResultData;
import com.sky.cim.entity.CompanyInfoEntity;
import com.sky.cim.mapper.CompanyInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CompanyInfoService extends ServiceImpl<CompanyInfoMapper, CompanyInfoEntity> {
    @Resource
    private CompanyInfoMapper companyInfoMapper;

    public ResultData getCompanyInfoList(HttpServletRequest request, String companyName, String type, String acceptDepartment, String pageNo, String pageSize) {
        try {
            long current = Long.parseLong(pageNo);
            long size = Long.parseLong(pageSize);
            QueryWrapper<CompanyInfoEntity> queryWrapper = new QueryWrapper<>();
            if (StringUtils.isNotBlank(companyName)) {
                queryWrapper.like("company_name", "%".concat(companyName).concat("%"));
            }
            if (StringUtils.isNotBlank(type)) {
                queryWrapper.like("type", "%".concat(type).concat("%"));
            }
            if (StringUtils.isNotBlank(acceptDepartment)) {
                queryWrapper.like("accept_department", "%".concat(acceptDepartment).concat("%"));
            }
            Page<CompanyInfoEntity> page = new Page<>();
            if (current > 0 && size > 0) {
                page.setCurrent(current);
                page.setSize(size);
            }
//            Page<CompanyInfoEntity> companyInfoEntityPage = companyInfoMapper.selectPage(page, queryWrapper);
            Page<CompanyInfoEntity> companyInfoEntityPage = companyInfoMapper.selectCompanyInfoPage(page, companyName, type, acceptDepartment);
            return ResultData.success(companyInfoEntityPage);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.fail(e.getMessage());
        }
    }

    public ResultData getCompanyInfo(HttpServletRequest request, String companyId) {
        try {
            String tableText = companyInfoMapper.getCompanyInfo(Long.parseLong(companyId));
            return ResultData.success(tableText);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return ResultData.fail(e.getMessage());
        }
    }

    public ResultData getTypeList(HttpServletRequest request) {
        try {
            List<String> typeList = companyInfoMapper.getTypeList();
            List<String> collect = typeList.stream()
                    .filter(StringUtils::isNotBlank)
                    .collect(Collectors.toList());
            return ResultData.success(collect);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.fail(e.getMessage());
        }
    }

    public ResultData getAcceptDepartmentList(HttpServletRequest request) {
        try {
            List<String> acceptDepartmentList = companyInfoMapper.getAcceptDepartmentList();
            List<String> collect = acceptDepartmentList.stream()
                    .filter(StringUtils::isNotBlank)
                    .collect(Collectors.toList());
            return ResultData.success(collect);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultData.fail(e.getMessage());
        }
    }

}
