package com.sky.cim.utils;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentPBEConfig;
import org.springframework.stereotype.Component;

@Component
public class EncryptUtil {
    public static String encrypt(String password) {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        EnvironmentPBEConfig config = new EnvironmentPBEConfig();
        config.setAlgorithm("PBEWithMD5AndDES");          // 加密的算法，这个算法是默认的
        config.setPassword("cim");                        // 加密的密钥，随便自己填写
        standardPBEStringEncryptor.setConfig(config);
        return standardPBEStringEncryptor.encrypt(password);
    }

    public static String decrypt(String password) {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        EnvironmentPBEConfig config = new EnvironmentPBEConfig();
        config.setAlgorithm("PBEWithMD5AndDES");
        config.setPassword("cim");
        standardPBEStringEncryptor.setConfig(config);
        return standardPBEStringEncryptor.decrypt(password);
    }
}
