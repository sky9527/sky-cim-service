package com.sky.cim.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "企业信息实体类", description = "企业信息实体类")
@TableName(value = "cim_company_info")
public class CompanyInfoEntity {

    @ApiModelProperty(value = "企业主键id")
    @TableId(value = "company_id", type = IdType.INPUT)
    private Long companyId;

    @ApiModelProperty(value = "企业名称")
    @TableField(value = "company_name")
    private String companyName;

    @ApiModelProperty(value = "类型")
    @TableField(value = "type")
    private String type;

    @ApiModelProperty(value = "认证资质及等级")
    @TableField(value = "accredited_level")
    private String accreditedLevel;

    @ApiModelProperty(value = "类别")
    @TableField(value = "category")
    private String category;

    @ApiModelProperty(value = "受理单位")
    @TableField(value = "accept_department")
    private String acceptDepartment;

    @ApiModelProperty(value = "业务咨询电话")
    @TableField(value = "business_consult_tel")
    private String businessConsultTel;

    @ApiModelProperty(value = "监督举报电话")
    @TableField(value = "supervise_report_tel")
    private String superviseReportTel;

    @ApiModelProperty(value = "受理日期")
    @TableField(value = "accept_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date acceptDate;

    @ApiModelProperty(value = "当前办理状态")
    @TableField(value = "current_deal_status")
    private String currentDealStatus;

    @ApiModelProperty(value = "详情表单文本")
    @TableField(value = "table_text")
    private String tableText;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;
}
