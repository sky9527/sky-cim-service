package com.sky.cim.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "人员信息实体类", description = "人员信息实体类")
@TableName(value = "cim_personnel_info")
public class PersonnelInfoEntity {

    @ApiModelProperty(value = "人员信息主键id")
    @TableId(value = "personnel_id", type = IdType.AUTO)
    private Long personnelId;

    @ApiModelProperty(value = "姓名")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "性别")
    @TableField(value = "sex")
    private String sex;

    @ApiModelProperty(value = "年龄")
    @TableField(value = "age")
    private String age;

    @ApiModelProperty(value = "职称")
    @TableField(value = "title")
    private String title;

    @ApiModelProperty(value = "职称专业")
    @TableField(value = "title_major")
    private String titleMajor;

    @ApiModelProperty(value = "专业")
    @TableField(value = "major")
    private String major;

    @ApiModelProperty(value = "级别")
    @TableField(value = "level")
    private String level;

    @ApiModelProperty(value = "企业名称")
    @TableField(value = "company_name")
    private String companyName;

    @ApiModelProperty(value = "企业id")
    @TableField(value = "company_id")
    private Long companyId;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

    @ApiModelProperty(value = "类型")
    @TableField(exist = false)
    private String type;

    @ApiModelProperty(value = "认证资质及等级")
    @TableField(exist = false)
    private String accreditedLevel;

    @ApiModelProperty(value = "类别")
    @TableField(exist = false)
    private String category;

    @ApiModelProperty(value = "受理单位")
    @TableField(exist = false)
    private String acceptDepartment;

    @ApiModelProperty(value = "受理日期")
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date acceptDate;

    @ApiModelProperty(value = "当前办理状态")
    @TableField(exist = false)
    private String currentDealStatus;

}
