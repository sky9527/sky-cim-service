package com.sky.cim.vo;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class LocalComputerRoomVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "机房名称")
    private String idcName;

    @ApiModelProperty(value = "机房地址")
    private String idcAddress;

    @ApiModelProperty(value = "机房编号")
    private String idcId;

    @ApiModelProperty(value = "资产管理员")
    private List<JSONObject> owner;
}
