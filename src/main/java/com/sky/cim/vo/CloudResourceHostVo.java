package com.sky.cim.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigInteger;

@Data
public class CloudResourceHostVo {

    @ApiModelProperty(value = "内网IP地址")
    private String internalIp;

    @ApiModelProperty(value = "外网IP地址")
    private String externalIp;

    @ApiModelProperty(value = "主机名")
    private String hostname;

    @ApiModelProperty(value = "操作系统")
    private String operatingSystem;

    @ApiModelProperty(value = "实例ID:阿里云ID, vmwareid 资产编号，物理机的ID")
    private String instanceId;

    @ApiModelProperty(value = "机房编号:阿里云北京A M7")
    private String idcId;

    @ApiModelProperty(value = "主机类型:虚拟机 物理主机 未知")
    private String hostType;

    @ApiModelProperty(value = "CPU信息")
    private String cpuInfo;

    @ApiModelProperty(value = "内存大小(kb)")
    private BigInteger memorySize;

    @ApiModelProperty(value = "磁盘大小(kb)")
    private BigInteger diskSize;

    @ApiModelProperty(value = "负责人:IT自定义的角色")
    private String owner;

    @ApiModelProperty(value = "产品编码")
    private String productId;

    @ApiModelProperty(value = "固定标签(环境 状态)")
    private String tags;

    @ApiModelProperty(value = "自定义标签")
    private String optTags;

    @ApiModelProperty(value = "所属DNS ID")
    private Integer parentDns;

    @ApiModelProperty(value = "所属LB ID")
    private Integer parentLb;

    @ApiModelProperty(value = "所属WAF ID")
    private Integer parentWaf;

    @ApiModelProperty(value = "所属HOST ID")
    private Integer parentHost;

    @ApiModelProperty(value = "所属集群ID数组，json格式")
    private String clusterId;


}
