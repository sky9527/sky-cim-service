package com.sky.cim.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CloudResourceOracleVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "云环境:阿里云/金山云/自建")
    private String cloudEnv;

    @ApiModelProperty(value = "版本")
    private String version;

    @ApiModelProperty(value = "连接地址 阿里云：RDS连接串/自建：IP地址")
    private String connectAddress;

    @ApiModelProperty(value = "产品编码")
    private String productId;

    @ApiModelProperty(value = "固定标签(环境)")
    private String tags;

    @ApiModelProperty(value = "自定义标签")
    private String optTags;

    @ApiModelProperty(value = "所属HOST ID")
    private Integer parentHost;
}
