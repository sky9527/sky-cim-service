package com.sky.cim.vo;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class LocalComputerCabinetVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "机柜编号")
    private String rackId;

    @ApiModelProperty(value = "所属机房编号")
    private String idcId;

    @ApiModelProperty(value = "机柜U数")
    private Integer rackU;

    @ApiModelProperty(value = "机柜电量")
    private String rackPower;

    @ApiModelProperty(value = "资产管理员")
    private List<JSONObject> owner;
}
