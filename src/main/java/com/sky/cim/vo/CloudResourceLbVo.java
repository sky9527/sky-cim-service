package com.sky.cim.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author fanxin01
 * @since 2020-10-22
 */
@Data
public class CloudResourceLbVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "云环境:阿里云/金山云/F5")
    private String cloudEnv;

    @ApiModelProperty(value = "外网负载VIP")
    private String externalVip;

    @ApiModelProperty(value = "内网负载VIP")
    private String internalVip;

    @ApiModelProperty(value = "负载名称")
    private String name;

    @ApiModelProperty(value = "转发规则:协议 监听端口 目标IP 目标端口")
    private String rules;

    @ApiModelProperty(value = "产品编码")
    private String productId;

    @ApiModelProperty(value = "对应云环境的负载均衡ID")
    private String loadBalancerId;

    @ApiModelProperty(value = "固定标签(环境)")
    private String tags;

    @ApiModelProperty(value = "自定义标签")
    private String optTags;

    @ApiModelProperty(value = "所属DNS ID")
    private Integer parentDns;

    @ApiModelProperty(value = "所属WAF ID")
    private Integer parentWaf;
}
