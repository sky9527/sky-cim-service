package com.sky.cim.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class CmdbClusterVo {

    @ApiModelProperty(value = "ID")
    private Integer id;

    @ApiModelProperty(value = "集群名称")
    private String clusterName;

    @ApiModelProperty(value = "旧系统实例ID")
    private String instanceIdOld;

    @ApiModelProperty(value = "产品编码")
    private String productCode;

    @ApiModelProperty(value = "环境类型")
    private Integer envType;

}
