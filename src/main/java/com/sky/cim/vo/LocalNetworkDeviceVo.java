package com.sky.cim.vo;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class LocalNetworkDeviceVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "设备类型")
    private String deviceType;

    @ApiModelProperty(value = "IP地址")
    private String ipAddress;

    @ApiModelProperty(value = "带外管理IP")
    private String adminIp;

    @ApiModelProperty(value = "设备名称")
    private String hostName;

    @ApiModelProperty(value = "机房编号")
    private String idcId;

    @ApiModelProperty(value = "机柜编号")
    private String rackId;

    @ApiModelProperty(value = "机柜U数")
    private Integer rackU;

    @ApiModelProperty(value = "品牌")
    private String brandName;

    @ApiModelProperty(value = "型号")
    private String brandType;

    @ApiModelProperty(value = "序列号")
    private String snNumber;

    @ApiModelProperty(value = "资产编号")
    private String assetId;

    @ApiModelProperty(value = "购买时间")
    private String purchaseTime;

    @ApiModelProperty(value = "维保开始时间")
    private String warrantStartTime;

    @ApiModelProperty(value = "维保到期时间")
    private String warrantExpireTime;

    @ApiModelProperty(value = "价格")
    private BigDecimal purchasePrice;

    @ApiModelProperty(value = "合同编号")
    private String contractId;

    @ApiModelProperty(value = "资产管理员")
    private List<JSONObject> owner;

    @ApiModelProperty(value = "产品编码")
    private String productId;

    @ApiModelProperty(value = "固定标签(环境)")
    private String tags;

    @ApiModelProperty(value = "状态：1有效 2已删除")
    private Integer status;

    @ApiModelProperty(value = "自定义标签")
    private String optTags;
}
