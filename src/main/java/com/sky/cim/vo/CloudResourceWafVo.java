package com.sky.cim.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CloudResourceWafVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "云环境:阿里云/金山云/F5")
    private String cloudEnv;

    @ApiModelProperty(value = "外网域名")
    private String domainName;

    @ApiModelProperty(value = "域名解析的CNAME")
    private String cname;

    @ApiModelProperty(value = "后端对应的服务器地址,如SLB的外网IP")
    private String serverIp;

    @ApiModelProperty(value = "产品编码")
    private String productId;

    @ApiModelProperty(value = "固定标签(环境)")
    private String tags;

    @ApiModelProperty(value = "自定义标签")
    private String optTags;

    @ApiModelProperty(value = "所属DNS ID")
    private Integer parentDns;
}
