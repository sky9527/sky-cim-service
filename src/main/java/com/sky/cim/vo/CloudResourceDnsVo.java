package com.sky.cim.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 前端入参类
 * </p>
 *
 * @author fanxin01
 * @since 2020-10-22
 */
@Data
public class CloudResourceDnsVo {

    @ApiModelProperty(value = "ID")
    private Integer id;

    @ApiModelProperty(value = "外部DNS ID")
    private String systemId;

    @ApiModelProperty(value = "网络类型")
    private String netType;

    @ApiModelProperty(value = "域名")
    private String domainName;

    @ApiModelProperty(value = "记录类型")
    private String recordType;

    @ApiModelProperty(value = "记录值")
    private String recordValue;

    @ApiModelProperty(value = "产品编码")
    private String productId;

    @ApiModelProperty(value = "所属子项目")
    private String subProject;

    @ApiModelProperty(value = "请求单号")
    private String ticket;

    @ApiModelProperty(value = "固定标签(环境)")
    private String tags;

    @ApiModelProperty(value = "自定义标签")
    private String optTags;


}
