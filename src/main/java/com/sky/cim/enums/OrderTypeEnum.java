package com.sky.cim.enums;

public enum OrderTypeEnum {
    noneOrder,
    ageDesc,
    ageAsc,
    titleDesc,
    titleAsc,
    titleMajorDesc,
    titleMajorAsc,
    typeDesc,
    typeAsc,
    accreditedLevelDesc,
    accreditedLevelAsc,
    categoryDesc,
    categoryAsc,
    acceptDepartmentDesc,
    acceptDepartmentAsc,
    acceptDateDesc,
    acceptDateAsc,
    currentDealStatusDesc,
    currentDealStatusAsc;
}