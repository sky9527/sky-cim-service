package com.sky.cim.enums;

public enum StatusCodeEnum {
    /**
     * 成功
     */
    SUCCESS(1, "success"),
    /**
     * 失败
     */
    FAIL(0, "fail"),
    /**
     * 异常
     */
    EXECEPTION(-1, "exception");

    private final Integer statusCode;
    private final String desc;

    StatusCodeEnum(Integer statusCode, String desc) {
        this.statusCode = statusCode;
        this.desc = desc;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public String getDesc() {
        return desc;
    }
}
