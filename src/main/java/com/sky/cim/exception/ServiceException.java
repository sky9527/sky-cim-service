package com.sky.cim.exception;

import com.sky.cim.enums.StatusCodeEnum;

public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private final String msg;
    private int code = StatusCodeEnum.EXECEPTION.getStatusCode();

    public ServiceException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public ServiceException(StatusCodeEnum statusCodeEnum) {
        super(statusCodeEnum.getDesc());
        this.code = statusCodeEnum.getStatusCode();
        this.msg = statusCodeEnum.getDesc();
    }

    public ServiceException(StatusCodeEnum statusCodeEnum, String... condition) {
        super(statusCodeEnum.getDesc());
        this.code = statusCodeEnum.getStatusCode();
        String value = statusCodeEnum.getDesc();
        for (int i = 0; i < condition.length; i++) {
            value = value.replace("$" + i, condition[i]);
        }
        this.msg = value;
    }

    public ServiceException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public ServiceException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ServiceException(int code, String msg, Throwable e) {
        super(msg, e);
        this.code = code;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public int getCode() {
        return code;
    }
}