package com.sky.cim.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sky.cim.entity.CompanyInfoEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CompanyInfoMapper extends BaseMapper<CompanyInfoEntity> {
    @Select(value = "select distinct type from cim_company_info ")
    List<String> getTypeList();

    @Select(value = "select distinct accept_department from cim_company_info ")
    List<String> getAcceptDepartmentList();

    @Select(value = "select table_text from cim_company_table where company_id = #{companyId} ")
    String getCompanyInfo(@Param(value = "companyId") long companyId);

    @Select(value = {"<script>" +
            "select cci.* from cim_company_info cci " +
            "left join cim_company_info_new ccin on cci.company_id = ccin.company_id " +
            "where 1 = 1 " +
            "<if test='companyName != null and companyName != \"\"'> " +
            "and cci.company_name like concat('%' , #{companyName}, '%') or ccin.company_name like concat('%', #{companyName}, '%') " +
            "</if> " +
            "<if test='type != null and type != \"\"'> " +
            "and cci.type like concat('%', #{type}, '%') " +
            "</if> " +
            "<if test='acceptDepartment != null and acceptDepartment != \"\"'> " +
            "and cci.accept_department like concat('%', #{acceptDepartment}, '%') " +
            "</if> " +
            "order by cci.company_id desc" +
            "</script>"
    })
    Page<CompanyInfoEntity> selectCompanyInfoPage(Page<CompanyInfoEntity> page
            , @Param("companyName") String companyName
            , @Param("type") String type
            , @Param("acceptDepartment") String acceptDepartment);
}
