package com.sky.cim.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sky.cim.entity.PersonnelInfoEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface PersonnelInfoMapper extends BaseMapper<PersonnelInfoEntity> {

    @Select(value = {"<script> " +
            "select cpi.* , cci.* from cim_personnel_info cpi " +
            "left join cim_company_info cci on cpi.company_id = cci.company_id " +
            "where 1 = 1 " +
            "<if test = 'companyName != null and companyName != \"\"'> " +
            "and cpi.company_name like concat('%',#{companyName},'%') " +
            "</if> " +
            "<if test = 'name != null and name != \"\"'> " +
            "and cpi.name = #{name}" +
            "</if> " +
            "order by " +
            "<if test = 'orderType == \"noneOrder\"'> " +
            "cpi.name desc " +
            "</if> " +
            "<if test = 'orderType == \"ageDesc\"'> " +
            "cpi.age desc " +
            "</if> " +
            "<if test = 'orderType == \"ageAsc\"'> " +
            "cpi.age asc " +
            "</if> " +
            "<if test = 'orderType == \"titleDesc\"'> " +
            "cpi.title desc " +
            "</if> " +
            "<if test = 'orderType == \"titleAsc\"'> " +
            "cpi.title asc " +
            "</if> " +
            "<if test = 'orderType == \"titleMajorDesc\"'> " +
            "cpi.title_major desc " +
            "</if> " +
            "<if test = 'orderType == \"titleMajorAsc\"'> " +
            "cpi.title_major asc " +
            "</if> " +
            "<if test = 'orderType == \"typeDesc\"'> " +
            "cci.type desc " +
            "</if> " +
            "<if test = 'orderType == \"typeAsc\"'> " +
            "cci.type asc " +
            "</if> " +
            "<if test = 'orderType == \"accreditedLevelDesc\"'> " +
            "cci.accredited_level desc " +
            "</if> " +
            "<if test = 'orderType == \"accreditedLevelAsc\"'> " +
            "cci.accredited_level asc " +
            "</if> " +
            "<if test = 'orderType == \"categoryDesc\"'> " +
            "cci.category desc " +
            "</if> " +
            "<if test = 'orderType == \"categoryAsc\"'> " +
            "cci.category asc " +
            "</if> " +
            "<if test = 'orderType == \"acceptDepartmentDesc\"'> " +
            "cci.accept_department desc " +
            "</if> " +
            "<if test = 'orderType == \"acceptDepartmentAsc\"'> " +
            "cci.accept_department asc " +
            "</if> " +
            "<if test = 'orderType == \"acceptDateDesc\"'> " +
            "cci.accept_date desc " +
            "</if> " +
            "<if test = 'orderType == \"acceptDateAsc\"'> " +
            "cci.accept_date asc " +
            "</if> " +
            "<if test = 'orderType == \"currentDealStatusDesc\"'> " +
            "cci.current_deal_status desc " +
            "</if> " +
            "<if test = 'orderType == \"currentDealStatusAsc\"'> " +
            "cci.current_deal_status asc " +
            "</if> " +
            "</script> "
    })
    Page<PersonnelInfoEntity> selectPersonnelInfoPage(Page<PersonnelInfoEntity> page
            , @Param("companyName") String companyName
            , @Param("name") String name
            , @Param("orderType") String orderType);
}
