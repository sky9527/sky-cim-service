package com.sky.cim.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sky.cim.entity.UserInfoEntity;

public interface UserInfoMapper extends BaseMapper<UserInfoEntity> {
}
