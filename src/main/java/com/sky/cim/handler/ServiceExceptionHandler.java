package com.sky.cim.handler;

import com.sky.cim.bean.ResultData;
import com.sky.cim.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
@Slf4j
@ControllerAdvice(basePackages = {"com.sky.cim"})
public class ServiceExceptionHandler {

    public static final ThreadPoolExecutor THREAD_POOL_EXECUTOR = new ThreadPoolExecutor(2
            , 5
            , 30
            , TimeUnit.SECONDS
            , new ArrayBlockingQueue<>(100)
            , new ThreadPoolExecutor.AbortPolicy());

    @ExceptionHandler(value = ServiceException.class)
    @ResponseBody
    public ResultData errorHandler(ServiceException serviceException) {
        log.error(serviceException.getMsg());
        return ResultData.exception(serviceException.getMsg());
    }
}