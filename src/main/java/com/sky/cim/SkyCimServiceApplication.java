package com.sky.cim;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@MapperScan(basePackages = "com.sky.cim.mapper")
@EntityScan(basePackages = "com.sky.cim.entity")
@EnableEncryptableProperties
@EnableFeignClients
@EnableSwagger2
@EnableTransactionManagement
@EnableConfigurationProperties
@SpringBootApplication
public class SkyCimServiceApplication {

    public static void main(String[] args) {
        System.setProperty("jasypt.encryptor.password", "cim");
        SpringApplication.run(SkyCimServiceApplication.class, args);
    }

}
