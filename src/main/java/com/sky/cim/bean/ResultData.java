package com.sky.cim.bean;

import com.sky.cim.enums.StatusCodeEnum;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class ResultData {
    private Integer code;
    private String message;
    private Object data;

    public ResultData() {
    }

    public ResultData(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static ResultData success(Object data) {
        return new ResultData(StatusCodeEnum.SUCCESS.getStatusCode(), StatusCodeEnum.SUCCESS.getDesc(), data);
    }

    public static ResultData fail(Object data) {
        return new ResultData(StatusCodeEnum.FAIL.getStatusCode(), StatusCodeEnum.FAIL.getDesc(), data);
    }

    public static ResultData exception(Object data) {
        return new ResultData(StatusCodeEnum.EXECEPTION.getStatusCode(), StatusCodeEnum.EXECEPTION.getDesc(), data);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}