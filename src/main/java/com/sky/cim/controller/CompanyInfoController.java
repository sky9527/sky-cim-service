package com.sky.cim.controller;

import com.sky.cim.bean.ResultData;
import com.sky.cim.service.CompanyInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/companyInfo")
@Slf4j
@Api(value = "企业信息")
public class CompanyInfoController {
    @Resource
    private CompanyInfoService companyInfoService;

    @GetMapping(value = "/getCompanyInfoList")
    @ApiOperation(value = "获取企业信息列表"
            , produces = MediaType.APPLICATION_JSON_VALUE
            , notes = "companyName(企业名称),type(类型),acceptDepartment(受理单位),pageNo(当前页码),pageSize(每页条数)")
    public ResultData getCompanyInfoList(HttpServletRequest httpServletRequest
            , @RequestParam(required = false, value = "companyName") String companyName
            , @RequestParam(required = false, value = "type") String type
            , @RequestParam(required = false, value = "acceptDepartment") String acceptDepartment
            , @RequestParam(defaultValue = "1", value = "pageNo") String pageNo
            , @RequestParam(defaultValue = "20", value = "pageSize") String pageSize) {
        return companyInfoService.getCompanyInfoList(httpServletRequest, companyName, type, acceptDepartment, pageNo, pageSize);
    }

    @GetMapping(value = "/getCompanyInfo")
    @ApiOperation(value = "获取企业信息"
            , produces = MediaType.APPLICATION_JSON_VALUE
            , notes = "companyId(企业id)")
    public ResultData getCompanyInfo(HttpServletRequest httpServletRequest, @RequestParam(value = "companyId") String companyId) {
        return companyInfoService.getCompanyInfo(httpServletRequest, companyId);
    }

    @GetMapping(value = "/getTypeList")
    @ApiOperation(value = "获取申请类型列表"
            , produces = MediaType.APPLICATION_JSON_VALUE
            , notes = "")
    public ResultData getTypeList(HttpServletRequest httpServletRequest) {
        return companyInfoService.getTypeList(httpServletRequest);
    }

    @GetMapping(value = "/getAcceptDepartmentList")
    @ApiOperation(value = "获取受理单位列表"
            , produces = MediaType.APPLICATION_JSON_VALUE
            , notes = "")
    public ResultData getAcceptDepartmentList(HttpServletRequest httpServletRequest) {
        return companyInfoService.getAcceptDepartmentList(httpServletRequest);
    }

}
