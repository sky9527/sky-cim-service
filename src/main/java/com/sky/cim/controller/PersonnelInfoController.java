package com.sky.cim.controller;

import com.sky.cim.bean.ResultData;
import com.sky.cim.service.PersonnelInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/personnelInfo")
@Slf4j
@Api(value = "人员信息")
public class PersonnelInfoController {
    @Resource
    private PersonnelInfoService personnelInfoService;

    @GetMapping(value = "/getPersonnelInfoList")
    @ApiOperation(value = "获取人员信息列表"
            , produces = MediaType.APPLICATION_JSON_VALUE
            , notes = "companyName(企业名称),name(姓名),pageNo(当前页码),pageSize(每页条数)")
    public ResultData getPersonnelInfoList(HttpServletRequest httpServletRequest
            , @RequestParam(required = false) String companyName
            , @RequestParam(required = false) String name
            , @RequestParam String orderType
            , @RequestParam(defaultValue = "1") String pageNo
            , @RequestParam(defaultValue = "20") String pageSize) {
        return personnelInfoService.getPersonnelInfoList(httpServletRequest, pageNo, pageSize);
    }

}
