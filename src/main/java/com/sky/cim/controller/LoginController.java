package com.sky.cim.controller;

import com.alibaba.fastjson.JSONObject;
import com.sky.cim.bean.ResultData;
import com.sky.cim.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/admin")
@Slf4j
@Api(value = "登录")
public class LoginController {
    @Resource
    private LoginService loginService;

    @PostMapping(value = "/login")
    @ApiOperation(value = "登录"
            , produces = MediaType.APPLICATION_JSON_VALUE
            , notes = "username(用户名administrator),password(密码123456)")
    public ResultData login(HttpServletRequest httpServletRequest, @RequestBody JSONObject jsonObject) {
        return loginService.login(httpServletRequest, jsonObject);
    }

    @PostMapping(value = "/logout")
    @ApiOperation(value = "退出登录", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResultData logout(HttpServletRequest httpServletRequest) {
        return loginService.logout(httpServletRequest);
    }

    @PostMapping(value = "/checkLogin")
    @ApiOperation(value = "校验登录", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResultData checkLogin(HttpServletRequest request) {
        return loginService.checkLogin(request);
    }
}
